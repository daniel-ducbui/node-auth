/** @format */

// Call base functions
const BaseRepository = require('./baseRepository')

// Interactive Model
const Hotel = require('../models/Hotels')

class HotelRepository extends BaseRepository {
  constructor() {
    super(Hotel) // Transfer interactive model
  }

  /**
   * Override if needed
   * @param {getAll}
   * @param {getSingle}
   * @param {store}
   * @param {update}
   * @param {destroy}
   */
}

module.exports = HotelRepository
