/** @format */

class BaseRepository {
  constructor(model) {
    console.log('>> Connected to BaseRepository')
    this.model = model
  }

  async getAll() {
    try {
      return await this.model.find({})
    } catch (error) {
      throw new Error(error)
    }
  }

  async getSingle(id) {
    try {
      return await this.model.findById({ _id: id })
    } catch (error) {
      throw new Error(error)
    }
  }

  async store(data) {
    try {
      return await this.model.create(data)
    } catch (error) {
      throw new Error(error)
    }
  }

  async update(_id, data) {
    try {
      return await this.model.findOneAndUpdate(_id, data)
    } catch (error) {
      throw new Error(error)
    }
  }

  async destroy(id) {
    try {
      return await this.model.findOneAndDelete({ _id: id })
    } catch (error) {
      throw new Error(error)
    }
  }
}

module.exports = BaseRepository
