/** @format */

// BaseController
const BaseController = require('./baseController')
// Module Repository
var RoomRepository = require('../repositories/roomRepository')
RoomRepository = new RoomRepository()
const { DataChecker } = require('../utils/checkers')

class RoomController extends BaseController {
  constructor() {
    super(RoomRepository)
  }

  /**
   * Override if needed
   *
   * Returned Data by default
   * @param {getAll}
   * @param {getSingle}
   * @param {destroy}
   *
   * Returned Error by default
   * @param {store}
   * @param {update}
   */

  async store(req, res, next) {
    try {
      // Receive data
      // Standardization
      const {
        name,
        hotel_id,
        immges,
        type,
        capacity,
        no_bunk,
        no_guest,
        facilities,
        policy,
        price,
        square,
      } = req.body

      if (no_bunk instanceof Object) {
        if (no_bunk.single < 1 && no_bunk.double < 1) {
          throw new Error(`no_bunk is required`)
        }
      } else {
        throw new Error(`no_bunk is required`)
      }

      const check = DataChecker({
        name,
        hotel_id,
        type,
        capacity,
        no_guest,
        price,
      })

      if (check.status) {
        const room = {
          name,
          hotel_id,
          immges,
          type,
          capacity,
          no_bunk,
          no_guest,
          facilities,
          policy,
          price,
          square,
        }

        await RoomRepository.store(room)
        res.status(200).send('Stored')
      } else {
        throw new Error(`${check.message} is required`)
      }
    } catch (error) {
      next(error)
    }
  }

  async update(req, res, next) {
    try {
      // Get data
      const { id } = req.params
      const {
        name,
        hotel_id,
        immges,
        type,
        capacity,
        no_bunk,
        no_guest,
        facilities,
        policy,
        price,
        square,
      } = req.body

      const check = DataChecker({
        name,
        hotel_id,
        type,
        capacity,
        no_guest,
        price,
      })

      if (no_bunk instanceof Object) {
        if (no_bunk.single < 1 && no_bunk.double < 1) {
          throw new Error(`no_bunk is required`)
        }
      } else {
        throw new Error(`no_bunk is required`)
      }

      if (check.status) {
        await RoomRepository.update(
          { _id: id },
          {
            name,
            hotel_id,
            immges,
            type,
            capacity,
            no_bunk,
            no_guest,
            facilities,
            policy,
            price,
            square,
            updatedDate: new Date(Date.now()),
          }
        )
        res.status(200).send('Updated')
      } else {
        throw new Error(`${check.message} is required`)
      }
    } catch (error) {
      error.status = 400
      next(error)
    }
  }

  async addRoom(req, res, next) {
    try {
      const { id } = req.params
      const query = req.query

      await RoomRepository.addRoom({ _id: id }, query.capacity)

      res.status(200).send('Added')
    } catch (error) {
      error.status = 400
      next(error)
    }
  }

  async findRoomByHotelId(req, res, next) {
    try {
      const query = req.query
      console.log(query.id)
      const rooms = await RoomRepository.findRoomByHotelId(query.id)
      res.status(200).json({ data: rooms })
    } catch (error) {
      error.status = 400
      next(error)
    }
  }
}

module.exports = RoomController
