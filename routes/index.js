/** @format */

// Middlewares
const verifyToken = require('../middlewares/verifyToken')
const { isAdmin, isUser } = require('../middlewares/verifyAdminPermission')

var authRouter = require('./auth')
var postRouter = require('./posts')
var hotelRouter = require('./hotels')
var userRouter = require('./users')
var roomRouter = require('./rooms')

const Router = (app) => {
  app.use('/auth', authRouter)
  app.use('/posts', [verifyToken], postRouter)
  app.use('/hotels', hotelRouter)
  app.use('/rooms', roomRouter)
  // app.use('/users', [verifyToken, isUser], userRouter)
}

module.exports = Router
