/** @format */

// BaseRouter
const BaseRouter = require('./baseRouter')

const router = require('express').Router()
// Module Controller
var hotelController = require('../controllers/hotelController')
// Check Module Controller
if (typeof hotelController === 'function') {
  hotelController = new hotelController()
}

class HotelRouter extends BaseRouter {
  constructor() {
    super(hotelController, router)
  }
}

const Router = new HotelRouter()

// Init base routers or overrided routers
Router.getAll()
Router.store()
Router.getSingle()
Router.update()
Router.destroy()

module.exports = router
