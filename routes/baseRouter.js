/** @format */

class BaseRouter {
  constructor(Controller, router) {
    console.log('>> Connected to BaseRouter')
    this.Controller = Controller
    this.router = router
  }

  getAll() {
    this.router.get('/', (req, res, next) =>
      this.Controller.getAll(req, res, next)
    )
  }

  store() {
    this.router.post('/', (req, res, next) =>
      this.Controller.store(req, res, next)
    )
  }

  getSingle() {
    this.router.get('/:id', (req, res, next) =>
      this.Controller.getSingle(req, res, next)
    )
  }

  update() {
    this.router.put('/:id', (req, res, next) =>
      this.Controller.update(req, res, next)
    )
  }

  destroy() {
    this.router.delete('/:id', (req, res, next) =>
      this.Controller.destroy(req, res, next)
    )
  }
}

module.exports = BaseRouter
