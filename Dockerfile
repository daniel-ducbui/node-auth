FROM node:alpine3.12

ENV NODE_ENV development
WORKDIR /api

COPY ./package.json ./
RUN npm i

COPY ./ . 
RUN rm -rf package-lock.json package-lock.json_backup

EXPOSE 8000
ENTRYPOINT npm run dev